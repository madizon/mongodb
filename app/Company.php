<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Company extends Model
{
    protected $connection = 'mongodb';
    protected $collection = 'companies';
    
    protected $fillable = [
        'name', 'website','email','employee_count','status'
    ];
}
